#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>
#include <sleep.h>

void signal_handler(int param);

int main(void)
{
    signal(SIGABRT, signal_handler);
    signal(SIGFPE, signal_handler);
    signal(SIGILL, signal_handler);
    signal(SIGINT, signal_handler);
    signal(SIGSEGV, signal_handler);
    signal(SIGTERM, signal_handler);

    if (errno)
    {
        fprintf(stderr, "Failed to register SIGSEGV signal handler");
        exit(EXIT_FAILURE);
    }
    
    for (size_t i = 10; i > 0; i--)
    {
        printf("Doing a buffer overflow in %d seconds.\n", i);
        fflush(stdin);
        sleep(1);
    }
    
    int ptr[16];
    ptr[2000] = 42;

    return EXIT_SUCCESS;
}

void signal_handler(int param)
{
    fprintf(stderr, "Signal detected!\n");
    switch(param)
    {
        case SIGABRT:
            fprintf(stderr, "Signal: Abort\n");
            break;
        case SIGFPE:
            fprintf(stderr, "Signal: Floating-Point Exception\n");
            break;
        case SIGILL:
            fprintf(stderr, "Signal: Illegal Instruction\n");
            break;
        case SIGINT:
            fprintf(stderr, "Signal: Interrupt\n");
            break;
        case SIGSEGV:
            fprintf(stderr, "Signal: Segmentation Violation\n");
            break;
        case SIGTERM:
            fprintf(stderr, "Signal: Terminate\n");
            break;
    }
    exit(EXIT_FAILURE);
}
