
CC = gcc
CFLAGS = -O3

all: buffer_overflow null_pointer_assignment stack_overflow

clean:
	rm -r release/objects

%.o: src/%.c
	mkdir -p release/objects
	$(CC) $(CLAGS) -Ilib -c -o release/objects/$@ $<

%: %.o
	mkdir -p release/exes
	$(CC) $(CLAGS) -o release/exes/$@ release/objects/$<
