#ifndef SLEEP_H
#define SLEEP_H

#if __has_include(<unistd.h>)
    #include <unistd.h>
#else
    #include <time.h>

    void sleep(double s);

    void sleep(double s)
    {
        time_t cur_time = time(NULL);
        while ((difftime(time(NULL), cur_time)) < s);
    }
#endif

#endif